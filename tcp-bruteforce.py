"""
Penetration Testing Tool Kit v1.0

TCP Server Fuzzing and Brute Force Tool

This script can be modified for fuzzing, brute forcing of services
and more. This is a basic TCP client ready for war.
"""


__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft (c) 2017'
__license__ = 'GNU GPL'


import getopt, sys, socket, time


global target_host, target_port, wordlist, speed, line


line = "\n-------------------------------------------------\n"


def usage():
    """
    usage

    Displays program usage information
    """
    print('{} - TCP Fuzzing and Brute Force Tool'.format(sys.argv[0]))
    print('{} -w <wordlist> -t <target> -p <port>'.format(sys.argv[0]))
    print('-w <wordlist>    : Wordlist of passwords or fuzzing strings')
    print('-t <target>      : Target host')
    print('-p <port>        : Target port')
    print('-s <speed>       : Connection rate in seconds')


def main(argv):
    """
    main

    @param argv:
    @return:
    """
    if len(sys.argv) != 9:
        usage()
        sys.exit(1)

    print("{} Penetration Testing Tool Kit v1.0 \n TCP Fuzzer / Brute Force {}".format(line, line))

    try:
        opts, args = getopt.getopt(argv, "h:w:t:p:s:", ["wordlist=", "target=", "port=", "speed="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-w", "--passwords"):
            try:
                wordlist = open(str(arg), "r").readlines()
            except(IOError):
                print("Error: Check your wordlist path\n")
                sys.exit(1)
        elif opt in ("-t", "--target"):
            target_host = str(arg)
        elif opt in ("-p", "--port"):
            target_port = int(arg)
        elif opt in ("-s", "--speed"):
            speed = int(arg)

    for w in wordlist:
        time.sleep(speed)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((target_host, target_port))
        client.send(w)
        response = client.recv(4096)

        # Expected response
        if "ACK" in response:
            print("[*] Found password: {}".format(w))
            sys.exit(0)
        else:
            print("[*] Invalid password: {}".format(w))



if __name__ == "__main__":
    """
    init

    Initialize the script application's main function
    """
    main(sys.argv[1:])

