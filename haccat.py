"""
Penetration Testing Tool Kit v1.0

Haccat is a Netcat replacement written in Python with additional features
related to penetration testing
"""


__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft (c) 2017'
__license__ = 'GNU GPL'


import sys, socket, getopt, threading, subprocess

"""
Global variables
"""
listen              = False
command             = False
upload              = False
execute             = ""
target              = ""
upload_destination  = ""
port                = 0


def usage():
    print("Haccat Net Tool\n")
    print("Usage: haccat.py -t target_host -p port")
    print("-l --listen              - listen on [host]:[port] for incoming connections")
    print("-e --execute=file_to_run - execute the given file upon receiving a connection")
    print("-c --command             - initialize a command shell")
    print("-u --upload=destination  - upon receiving a connection upload file adn write to [destination]\n\n")
    print("Examples:")
    print("haccat.py -t 192.168.0.1 -p 55555 -l -c")
    print("haccat.py -t 192.168.0.1 -p 5555 -l -u=c:\\target.exe")
    print("haccat.py -t 192.168.0.1 -p 5555 -l -e=\"cat /etc/passwd\"")
    print("echo 'AAAA' | ./haccat.py -t 192.168.0.1 -p 135")
    sys.exit(0)


def client_sender(buffer):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to target host
        client.connect((target, port))

        if len(buffer):
            client.send(buffer)

        while True:
            # wait for data
            recv_len = 1
            response = ""

            while recv_len:
                data = client.recv(4096)
                recv_len = len(data)
                response += data

                if recv_len < 4096:
                    break


            print(response)

            buffer = raw_input("")
            buffer += "\n"

            client.send(buffer)
    except:
        print("[*] Exception! Exiting.")

        client.close()


def server_loop():
    global target

    # if no target, listen on all interfaces
    if not len(target):
        target = "0.0.0.0"

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((target, port))

    server.listen(5)

    while True:
        client_socket, addr = server.accept()

        # spin off thread to handle client
        client_thread = threading.Thread(target=client_handler, args=(client_socket,))
        client_thread.start()


def run_command(command):
    # trim newline
    command = command.rstrip()

    # run command and return output
    try:
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
    except:
        output = "Failed to execute command.\n"

    # send output back to client
    return output


def client_handler(client_socket):
    global upload, execute, command

    # Check for upload
    if len(upload_destination):
        # read in all bytes and write to dest
        file_buffer = ""

        # keep reading until none
        while True:
            data = client_socket.recv(1024)

            if not data:
                break
            else:
                file_buffer += data

        # write out the received bytes
        try:
            file_descriptor = open(upload_destination, "wb")
            file_descriptor.write(file_buffer)
            file_descriptor.close()

            # acknowledge that file was written
            client_socket.send("Successfully saved file to {}".format(upload_destination))
        except:
            client_socket.send("Failed to save file to {}".format(upload_destination))


    # check for command execution
    if len(execute):
        # run command
        output = run_command(execute)
        client_socket.send(output)

    # command shell
    if command:
        while True:
            # show prompt
            client_socket.send("<HC:#> ")
            cmd_buffer = ""

            while "\n" not in cmd_buffer:
                cmd_buffer += client_socket.recv(1024)

            # send back prompt
            response = run_command(cmd_buffer)

            # send response
            client_socket.send(response)


def main():
    global listen, port, execute, command, upload_destination, target

    if not len(sys.argv[1:]):
        usage()

    # Read command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hle:t:p:u:c", ["help", "listen", "execute", "target", "port",
                                                                 "command", "upload"])
    except getopt.GetoptError as err:
        print(str(err))
        usage()

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        elif o in ("-l", "--listen"):
            listen = True
        elif o in ("-e", "--execute"):
            execute = a
        elif o in ("-c", "--commandshell"):
            command = True
        elif o in ("-u", "--upload"):
            upload_destination = a
        elif o in ("-t", "--target"):
            target = a
        elif o in ("-p", "--port"):
            port = int(a)
        else:
            assert False, "Unhandled Option"

    # Are we going to listen or send data from stdin?
    if not listen and len(target) and port > 0:
        # Read in the buffer from the commandline
        # This will block, so send CTRL+D if not sending input
        # to stdin
        buffer = sys.stdin.read()

        # Send data
        client_sender(buffer)

    # We are going to listen, and potentially
    # upload things, execute commands, and drop a shell back
    # depending on the command line options above
    if listen:
        server_loop()


main()