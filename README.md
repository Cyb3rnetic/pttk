# Penetration Testing Tool Kit v1.0

## Quick and Dirty

The Penetration Testing Tool Kit is combined of a collection of quick-and-dirty standalone scripts that can be used and modified during penetration tests.
This kit aims to be combined with the common tools every red team member should keep in his toolbox for performing various operations against targets.


Requires Python v2.7


